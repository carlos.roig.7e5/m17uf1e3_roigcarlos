using UnityEngine;
using Scripts;

public class GetPlayerState : MonoBehaviour
{
    private State _playerState;

    // Update is called once per frame
    void Update()
    {
        _playerState = GameObject.Find("Player").GetComponent<PlayerData>().Life;
    }
}
