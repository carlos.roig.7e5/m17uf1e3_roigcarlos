using UnityEngine;
using UnityEngine.UI;
public class FinishGameText : MonoBehaviour
{
    private GameManager gameManager;
    private GameObject _playerName;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _playerName = GameObject.Find("PlayerName");
        if (gameManager.GameFinish == GameFinish.Win)
            gameObject.GetComponent<TMPro.TMP_Text>().text = "Felicidades jugador " + _playerName.GetComponent<Text>().text + ". Has ganado.";
        else
            gameObject.GetComponent<TMPro.TMP_Text>().text = "L�stima jugador " + _playerName.GetComponent<Text>().text + ". Has perdido.";
    }
}
