using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    private float _timer;
    private float _second;
    // Start is called before the first frame update
    void Start()
    {
        _timer = 180;
        _second = 0;
        ShowTimer(CalculateTimer());
    }

    // Update is called once per frame
    void Update()
    {
        if (1 <= _second)
        {
            ShowTimer(CalculateTimer());
            _second = 0;
            _timer -= 1;
        }
        else
            _second += Time.deltaTime;
    }

    void ShowTimer(string timer)
    {
        gameObject.GetComponent<TMP_Text>().text = timer;
    }

    string CalculateTimer()
    {
        float timer = _timer / 60;
        string stringTimer = timer.ToString();
        string minuteTimer = stringTimer[0].ToString();
        string secondTimer = "";
        for (int i = 0; i < stringTimer.Length; i++)
        {
            if (i == 0)
                secondTimer += "0";
            else
            {
                secondTimer += stringTimer[i];
            }
        }
        secondTimer = (System.Math.Round(System.Convert.ToDouble(secondTimer) * 60)).ToString();
        minuteTimer = minuteTimer.Insert(0, "0");
        if (secondTimer.Length < 2)
        {
            secondTimer = secondTimer.Insert(0, "0");
        }
        return minuteTimer + ":" + secondTimer;
    }

    public bool TimerEnd()
    {
        if (_timer <= 0)
            return true;
        return false;
    }
}
