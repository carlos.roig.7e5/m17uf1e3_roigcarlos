using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pop_Up : MonoBehaviour
{
    private GameObject _popUp;
    // Start is called before the first frame update
    void Start()
    {
        _popUp = GameObject.Find("Pop_Up");
        _popUp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartPop_Up()
    {
        _popUp.SetActive(true);
        StartCoroutine(CloseAfterTime(1.5f));
    }

    public IEnumerator CloseAfterTime(float t)
    {
        yield return new WaitForSeconds(t);
        _popUp.SetActive(false);
    }
}
