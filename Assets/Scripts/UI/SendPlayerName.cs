using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SendPlayerName : MonoBehaviour
{
    private TMP_Text _playerName;
    private Text _sendPlayerName;
    // Start is called before the first frame update
    void Start()
    {
        _playerName = GameObject.Find("Name").GetComponent<TMP_Text>();
        _sendPlayerName = gameObject.GetComponent<Text>();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        _sendPlayerName.text = _playerName.text;
    }
}
