using UnityEngine;

namespace Scripts
{
    public enum Movement
    {
        Move,
        Jump,
        Stop,
        Falling
    }

    public enum Stat
    {
        Normal,
        Giant
    }
    public class PlayerMovement : MonoBehaviour
    {
        private SpriteRenderer _sr;
        [SerializeField]
        private float _counter;
        [SerializeField]
        private float _frameRate;
        public float FrameRate
        {
            get => _frameRate;
            set => _frameRate = value;
        }

        private PlayerData _playerData;
        [SerializeField]
        private Movement _movement;
        private Rigidbody2D _rb;
        private Animator _animator;
        private GameManager _gameManager;
        // Start is called before the first frame update
        void Start()
        {
            _sr = gameObject.GetComponent<SpriteRenderer>();
            _counter = 0;
            _movement = Movement.Stop;
            _playerData = gameObject.GetComponent<PlayerData>();
            _rb = gameObject.GetComponent<Rigidbody2D>();
            _animator = gameObject.GetComponent<Animator>();
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_playerData.Life == State.Alive)
            {
                float direction;
                if (_playerData.Speed > 0)
                    direction = 1;
                else
                    direction = -1;
                switch (_movement)
                {
                    case Movement.Stop:
                        _animator.SetBool("Walk", false);
                        _animator.SetBool("Jump", false);
                        break;
                    case Movement.Move:
                        _animator.SetBool("Walk", true);
                        break;
                    case Movement.Jump:
                            _animator.SetBool("Jump", true);
                        break;
                }
                if (_frameRate != 0)
                {
                    if (_counter >= 1 / _frameRate)
                    {
                        if (_movement != Movement.Jump)
                        {
                           
                            if (_gameManager.ModeDeJoc == ModeDeJoc.Player)
                            {
                                FlipChar(direction*Input.GetAxis("Horizontal"));
                                ManualChangePositionBySpeed(_playerData.Speed);
                                Jump(_playerData.JumpForce, _playerData.Speed);
                            }
                            else
                            {
                                FlipChar(direction);
                                AutomaticChangePositionBySpeed(_playerData.Speed);
                                ChangePosition(direction);
                            }
                        }
                    }
                    if (_counter >= 1 / _frameRate)
                        _counter = 0;
                    _counter += Time.deltaTime;
                }
            }
        }
        void ChangePosition(float direction)
        {
            var raycastHit = Physics2D.Raycast(new Vector2(transform.position.x + 0.1f*direction, transform.position.y), new Vector2(direction, -1));
            if (raycastHit.transform == null)
                 _playerData.Speed *= -1;
        }

       

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (_movement == Movement.Jump && collision.gameObject.layer == 3)
            {
                _movement = Movement.Stop;
            }
        }

        void AutomaticChangePositionBySpeed(float speed)
            {
                transform.position = new Vector3(transform.position.x + speed * _counter, transform.position.y, transform.position.z);
            }

            void ManualChangePositionBySpeed(float speed)
            {
                    transform.position += new Vector3(Input.GetAxis("Horizontal")*speed*_counter,0, transform.position.z);   
            }

            void Jump(float force, float speed)
            {
            if (Input.GetKey(KeyCode.Space))
            {
                _rb.AddForce(new Vector2(Input.GetAxis("Horizontal")*speed*_rb.mass, force), ForceMode2D.Impulse);
                _movement = Movement.Jump;
            }
            }

            void FlipChar(float direction)
            {
            //Turn to left
            if (direction<0)
                ChangePosition(Movement.Move, true);
            else if (direction>0)
                ChangePosition(Movement.Move, false);
            //Turn to right
            else
                ChangePosition(Movement.Stop, _sr.flipX);
            }

        void ChangePosition(Movement axis, bool flipDirection)
        {
            _movement = axis;
            _sr.flipX = flipDirection;
        }

    }
    }

