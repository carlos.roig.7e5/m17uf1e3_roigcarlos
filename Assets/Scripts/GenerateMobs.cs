using UnityEngine;

public class GenerateMobs : MonoBehaviour
{
    [SerializeField]
    private GameObject _prefabFireball;
    [SerializeField]
    private GameObject _prefabMushroom;
    private Vector3 _objectPosition;
    private float _yPosition;
    private float _xPosition;
    private Vector3 _mousePosition;
    [SerializeField]
    private float _maxXvalue;
    [SerializeField]
    private float _minXValue;
    private float _spawnMushroom;
    private float _spawnFireBall;
    private float _fireTimer;
    private float _mushroomTimer;
    private GameObject _fireBall;
    private GameObject _mushroom;
    private GameManager _gameManager;
    [SerializeField]
    private Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        _yPosition = transform.position.y;
        _spawnFireBall = 0.5f;
        _fireTimer = 0;
        _spawnMushroom = 10;
        _mushroomTimer = _spawnMushroom;
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.ModeDeJoc == ModeDeJoc.Player)
        {
            _xPosition = (float)RandomValue(_minXValue, _maxXvalue);
            SetTheMobSpawnPosition();
        }
        else
        {
            _mousePosition = Input.mousePosition;
            _objectPosition = camera.ScreenToWorldPoint(_mousePosition);
            _xPosition = _objectPosition.x;
            if (Input.GetMouseButton(0))
                SetTheMobSpawnPosition();
        }
    }

    void SetTheMobSpawnPosition()
    {
        SpawnMob(ref _fireTimer, _spawnFireBall, _prefabFireball, ref _fireBall, new Vector3(_xPosition, _yPosition));
        SpawnMob(ref _mushroomTimer, _spawnMushroom, _prefabMushroom, ref _mushroom, new Vector3(_xPosition, _yPosition));
    }

    void SpawnMob(ref float timer, float maxSpawnTime, GameObject prefab, ref GameObject gameObject, Vector3 position)
    {
        if (timer <= 0)
        {
            gameObject = Instantiate(prefab, position, Quaternion.identity);
            timer = maxSpawnTime;
        }
        else
            timer -= Time.deltaTime;
    }

   double RandomValue(float min, float max)
    {
        var rnd = new System.Random();
        return rnd.NextDouble()*(max-min) + min;
    }
}
