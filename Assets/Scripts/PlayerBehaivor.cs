using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scripts;

public class PlayerBehaivor : MonoBehaviour
{
    [SerializeField]
    private float _waitTime;
    [SerializeField]
    private float _countdown;
    private float _habTimeLimit;
    private PlayerData _playerData;
    private Rigidbody2D _rb;
    private Stat _stat;
    [SerializeField]
    private float _frameRate;
    public float FrameRate
    {
        get => _frameRate;
        set => _frameRate = value;
    }
    [SerializeField]
    private float _counter;
    public float HabTimeLimit
    {
        get => _habTimeLimit;
        set => _habTimeLimit = value;
    }
    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _countdown = 0;
        _counter = 0;
        _frameRate = 12;
        _habTimeLimit = 10;
        _stat = Stat.Normal;
        _playerData = gameObject.GetComponent<PlayerData>();
        _playerData.InitHeight = _playerData.Height;
        _playerData.GiantHeight = _playerData.InitHeight * _playerData.AugHeight;
        _animator = gameObject.GetComponent<Animator>();
        _rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsFalling())
         StartCoroutine(DeathAfterTime(1f));
        
        if (_playerData.Life == State.Alive)
        {

            if (_frameRate != 0)
            {
                if (_counter >= 1 / _frameRate)
                {
                    GiantHability();
                    _counter = 0;
                }
              else
                _counter += Time.deltaTime;
            }
        }
        else
            OnDeath();
    }

    public void PlayerActiveGiant()
    {
        if (_countdown <= 0)
            _stat = Stat.Giant;
    }
    void GiantHability()
    {
        if (_stat == Stat.Giant)
        {
            Tranform(0.2f, 1 / _frameRate, _playerData.Height, _playerData.GiantHeight);
            if (_countdown >= _habTimeLimit)
                _stat = Stat.Normal;
        }
        if (_stat == Stat.Normal)
            Tranform(-0.2f, -1 / FrameRate, _playerData.InitHeight, _playerData.Height);
    }
    void Tranform(float mod, float time, float minorValue, float maxValue)
    {
        if (minorValue < maxValue)
        {
            ChangeVariable(mod, ref _playerData.Height);
            ChangeVariable(mod, ref _playerData.Weight);
            transform.localScale = new Vector3(_playerData.Height, _playerData.Height, transform.localScale.z);
        }
        else
        {
            _countdown += time;
        }
    }
    void ChangeVariable(float mod, ref float variable)
    {
        variable += mod;
    }

    public void ExplosionForce(float force)
    {
        _rb.AddForce(new Vector2(force, force / 2), ForceMode2D.Impulse);
    }

    void OnDeath()
    {
        _animator.SetBool("Death", true);
    }

    bool IsFalling()
    {
        var raycastHit = Physics2D.Raycast(new Vector2(transform.position.x+0.5f, transform.position.y+0.5f), Vector2.down);
        if (raycastHit.transform != null)
            return false;
        
        return true;
    }
    public IEnumerator DeathAfterTime(float t)
    {
        yield return new WaitForSeconds(t);
        _playerData.Life = State.Death;
    }
}
