using UnityEngine;
using UnityEngine.SceneManagement;
public enum Escenas
{
    StartScreen,
    GameScreen,
    GameOverScreen
}

public enum ModeDeJoc
{
    Player,
    Fire
}

public enum GameFinish
{
    Win,
    Lose
}
public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                Debug.LogError("Game Manager is NULL");
            }
            return _instance;
        }
    }
    private Escenas _scene;
    public ModeDeJoc ModeDeJoc;
    public GameFinish GameFinish;
    private void Awake()
    {
        _instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ChangeScene();
        if(_scene==Escenas.GameScreen)
        if(GameObject.Find("Player").GetComponent<Scripts.PlayerData>().Life == Scripts.State.Death||GameObject.Find("Timer").GetComponent<Timer>().TimerEnd())
        {
                switch (GameObject.Find("Player").GetComponent<Scripts.PlayerData>().Life)
                {
                    case Scripts.State.Alive:
                        if (ModeDeJoc == ModeDeJoc.Player)
                            GameFinish = GameFinish.Win;
                        else GameFinish = GameFinish.Lose;
                        break;
                    case Scripts.State.Death:
                        if (ModeDeJoc == ModeDeJoc.Player)
                            GameFinish = GameFinish.Lose;
                        else GameFinish = GameFinish.Win;
                        break;
                }
                SceneManager.LoadScene("GameOver");
        }
        if (_scene == Escenas.StartScreen)
        {
            switch (GameObject.Find("Dropdown").GetComponent<TMPro.TMP_Dropdown>().options[GameObject.Find("Dropdown").GetComponent<TMPro.TMP_Dropdown>().value].text)
            {

                case "GameMode2":
                    ModeDeJoc = ModeDeJoc.Fire;
                    break;
                default:
                    ModeDeJoc = ModeDeJoc.Player;
                    break;
            }
        }
    }

    void ChangeScene()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Game":
                _scene = Escenas.GameScreen;
                break;
            case "GameOver":
                _scene = Escenas.GameOverScreen;
                break;
            case "Menu":
                _scene = Escenas.StartScreen;
                break;
            default:
                break;
        }
    }
}
