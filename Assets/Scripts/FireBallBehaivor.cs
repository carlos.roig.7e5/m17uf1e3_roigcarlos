using UnityEngine;
using Scripts;
public class FireBallBehaivor : MonoBehaviour
{
    private Animator _animator;
    private PlayerBehaivor _playerbehaivour;
    [SerializeField]
    private float _explosionForce;
    private float[,] _forceApplication;
    private Rigidbody2D rigidbody2D;
    // Start is called before the first frame update
    void Start()
    {
        _animator = gameObject.GetComponent<Animator>();
        _playerbehaivour = GameObject.Find("Player").GetComponent<PlayerBehaivor>();
        _forceApplication = new float[,] { {0.2f,1, 0 }, {-0.2f,-1, 0 } };
        rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

            if (collision.gameObject.layer == 3)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionY;
                _animator.SetBool("Explosion", true);
            }
            if(collision.gameObject.name == "Player")
            switch (_animator.GetBool("Explosion"))
            {
                case true:
                        if (collision.gameObject.transform.position.x < transform.position.x)
                            _playerbehaivour.ExplosionForce(_explosionForce * -1);
                        else
                            _playerbehaivour.ExplosionForce(_explosionForce);
                    break;
                default:

                    collision.gameObject.GetComponent<PlayerData>().Life = State.Death;
                    DestroyObject();
                    break;
            }
      }

    private void OnTriggerStay2D(Collider2D collision)
    {
            if (collision.gameObject.layer == 3)
            {
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionY;
                _animator.SetBool("Explosion", true);
            }
        
        if (collision.gameObject.name == "Player")
            switch (_animator.GetBool("Explosion"))
            {
                case true:
                    if (collision.gameObject.name == "Player")
                    {
                        if (collision.gameObject.transform.position.x < transform.position.x)
                            _playerbehaivour.ExplosionForce(_explosionForce * -1);
                        else
                            _playerbehaivour.ExplosionForce(_explosionForce);
                    }
                    break;
                default:

                    collision.gameObject.GetComponent<PlayerData>().Life = State.Death;
                    DestroyObject();
                    break;
            }
    }
    
    private void DestroyObject()
    {
        Destroy(gameObject);
    }
}
