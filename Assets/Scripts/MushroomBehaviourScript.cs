using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomBehaviourScript : MonoBehaviour
{
    private float _Maxtime;
    private float _timer;
    // Start is called before the first frame update
    void Start()
    {
        _Maxtime = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (_timer >= _Maxtime)
            DestroyObject();
        else
            _timer += Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            DestroyObject();
            collision.gameObject.GetComponent<PlayerBehaivor>().PlayerActiveGiant();
        }
    }

    private void DestroyObject()
    {
        Destroy(gameObject);
    }
}
